// Lock client interface

#ifndef lock_client_h
#define lock_client_h

#include <string>

#include "lock_protocol.h"
#include "rpc.h"
#include "rsm_client.h"

#define __LOCK_CLIENT_SLEEP_TIME 50000

// Client interface to the lock server
class lock_client
{
protected:
  rsm_client *clt;
  enum xxcl_state {LOCKING};
  typedef int cl_state;
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  std::map<lock_protocol::lockid_t, cl_state> *locks;
  int clt_id;
  int seqno;
public:
  lock_client(std::string dst);
  virtual ~lock_client();

  virtual lock_protocol::status acquire(lock_protocol::lockid_t lid);
  virtual lock_protocol::status release(lock_protocol::lockid_t lid);
  virtual lock_protocol::status stat(lock_protocol::lockid_t lid);
};

#endif
