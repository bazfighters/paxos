// RPC stubs for clients to talk to lock_server

#include <sstream>
#include <iostream>
#include <stdio.h>
#include <arpa/inet.h>

#include "lock_client.h"
#include "rpc.h"

bool rand_seeded = false;

lock_client::lock_client(std::string dst)
{
  clt = new rsm_client(dst);
  locks = new std::map<lock_protocol::lockid_t, cl_state>();
  seqno = 0;
  assert(pthread_mutex_init(&mutex,NULL) == 0);
  assert(pthread_cond_init(&cond,NULL) == 0);
  if(__sync_bool_compare_and_swap(&rand_seeded, false, true)) {
      srand (time(NULL));
  }
  clt_id = random();
  printf("lock_client::lock_client: id is %d\n", clt_id);
}

int
lock_client::stat(lock_protocol::lockid_t lock_id)
{
  int r;
  int ret = clt->call(lock_protocol::stat, clt_id, lock_id, r);
  printf("lock_client::stat: stat for lock %llu requested.\n", lock_id);
  assert(ret == lock_protocol::OK);
  return r;
}

lock_protocol::status
lock_client::acquire(lock_protocol::lockid_t lock_id)
{
  int r;
  int ret;
  // check if the mutex should be including do while
  // in order to be the only one calling the primary
  //printf("lock_client::acquire: setting %llu lock to acquiring\n", lock_id);
  pthread_mutex_lock(&mutex);
  // check if it exists?
  while(locks->find(lock_id) != locks->end()) {
    pthread_cond_wait(&cond, &mutex);
  }
  int acquire_seqno = seqno;
  seqno++;
  (*locks)[lock_id] = LOCKING;
  pthread_mutex_unlock(&mutex);

  do {
    // set acquiring, none or locked state
    ret = clt->call(lock_protocol::acquire, clt_id, lock_id, acquire_seqno, r);
    //printf("lock_client::acquire: calling acquire on the server of lock %llu\n", lock_id);
    if(ret != lock_protocol::OK) {
      // set sleep time to 50 ms
      usleep(50000);
    }
  } while(ret != lock_protocol::OK);

  //printf("lock_client::acquire: setting lock %llu to locked\n", lock_id);
  assert(ret == lock_protocol::OK);
  return r;
}

lock_protocol::status
lock_client::release(lock_protocol::lockid_t lock_id)
{
  int r;
  //printf("lock_client::release: calling primary replica to release lock %llu\n", lock_id);
  int ret = clt->call(lock_protocol::release, clt_id, lock_id, r);

  // check if the state is locked?
  //printf("lock_client::release: setting lock %llu to unlocked\n", lock_id);
  pthread_mutex_lock(&mutex);
  // should it delete the lock from the locks map?
  std::map<lock_protocol::lockid_t,cl_state>::iterator it;
  if((it = locks->find(lock_id)) != locks->end()) {
    locks->erase(it);
    pthread_cond_broadcast(&cond);
  }
  pthread_mutex_unlock(&mutex);
  assert(ret == lock_protocol::OK);
  return r;
}

lock_client::~lock_client()
{
  pthread_cond_destroy(&cond);
  pthread_mutex_destroy(&mutex);
  delete locks;
  delete clt;
}