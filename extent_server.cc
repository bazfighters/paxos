// The extent server implementation

#include "extent_server.h"
#include <sstream>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void
extent::write(std::string &buf, unsigned long long off,
              extent_protocol::attr &a)
{
  if(off + buf.size() > this->buf.size()) {
    this->buf.resize(off + buf.size());
  }
  this->buf.replace(off, buf.size(), buf);

  time_t curr_time = time(NULL);
  this->attr.atime = curr_time;
  this->attr.mtime = curr_time;
  this->attr.ctime = curr_time;
  this->attr.size = this->buf.size();
  a = this->attr;
}

void
extent::read(std::string &buf, unsigned long long size,
             unsigned long long off)
{
  if(off > this->buf.size()) {
    buf = "";
    return;
  }

  if(size + off > this->buf.size()) {
    size = this->buf.size() - off;
  }

  buf = this->buf.substr(off, size);

  time_t curr_time = time(NULL);
  this->attr.atime = curr_time;
}

void
extent::get_attr(extent_protocol::attr &a)
{
  a = this->attr;
}

void
extent::set_attr(extent_protocol::attr &a, unsigned char mask,
                 extent_protocol::attr &attr)
{
  time_t curr_time = time(NULL);
  if(mask & EXTENT_SIZE_MASK) {
    this->buf.resize(a.size);
    this->attr.size = a.size;
    this->attr.mtime = curr_time;
    this->attr.atime = curr_time;
    this->attr.ctime = curr_time;
  }
  if(mask & EXTENT_ATIME_MASK) {
    this->attr.atime = a.atime;
  }
  if(mask & EXTENT_MTIME_MASK) {
    this->attr.mtime = a.mtime;
  }
  attr = this->attr;
}

extent_server::extent_server()
{
  extents = new std::map<extent_protocol::extentid_t, extent *>();
  assert(pthread_mutex_init(&mutex, NULL) == 0);

  extent_protocol::attr a;
  this->put(1, std::string(""), 0, a);
}

extent_server::~extent_server()
{
  delete extents;
  assert(pthread_mutex_destroy(&mutex) == 0);
}

int
extent_server::put(extent_protocol::extentid_t id, std::string buf,
                   unsigned long long off, extent_protocol::attr &a)
{
  printf("put ext %016llx\n", id);
  extent *e;
  {
    ScopedLock ml(&mutex);
    if((e = (*extents)[id]) == NULL) {
      (*extents)[id] = e = new extent();
    }
  }
  e->write(buf, off, a);
  printf("\ta=%u m=%u c=%u s=%u\n", a.atime, a.mtime, a.ctime, a.size);
  return extent_protocol::OK;
}

int
extent_server::get(extent_protocol::extentid_t id, unsigned long long sz,
                   unsigned long long off, std::string &buf)
{
  printf("get ext %016llx\n", id);
  extent *e;
  {
    ScopedLock ml(&mutex);
    if((e = (*extents)[id]) == NULL) {
      return extent_protocol::NOENT;
    }
  }
  e->read(buf, sz, off);
  return extent_protocol::OK;
}

int
extent_server::getattr(extent_protocol::extentid_t id, extent_protocol::attr &a)
{
  printf("getattr ext %016llx\n", id);
  extent *e;
  {
    ScopedLock ml(&mutex);
    if((e = (*extents)[id]) == NULL) {
      return extent_protocol::NOENT;
    }
  }
  e->get_attr(a);
  printf("\ta=%u m=%u c=%u s=%u\n", a.atime, a.mtime, a.ctime, a.size);
  return extent_protocol::OK;
}

int
extent_server::setattr(extent_protocol::extentid_t id,
                       extent_protocol::attr a, unsigned char mask,
                       extent_protocol::attr &attr)
{
  printf("setattr ext %016llx\n", id);
  extent *e;
  {
    ScopedLock ml(&mutex);
    if((e = (*extents)[id]) == NULL) {
      return extent_protocol::NOENT;
    }
  }
  e->set_attr(a, mask, attr);
  printf("\ta=%u m=%u c=%u s=%u mask=%d\n", a.atime, a.mtime,
         a.ctime, a.size, mask);
  return extent_protocol::OK;
}

int
extent_server::remove(extent_protocol::extentid_t id, int &)
{
  printf("remove ext %016llx\n", id);
  extent *e;
  {
    ScopedLock ml(&mutex);
    if((e = (*extents)[id]) == NULL) {
      return extent_protocol::NOENT;
    }
  }
  delete e;
  extents->erase(id);
  return extent_protocol::OK;
}
