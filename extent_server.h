// this is the extent server

#ifndef extent_server_h
#define extent_server_h

#include <string>
#include <map>
#include "extent_protocol.h"

#define EXTENTS_SZ (1 << 10)

class extent
{
public:
  extent() {}
  virtual ~extent() {}

  virtual void write(std::string &buf, unsigned long long off,
                     extent_protocol::attr &a);
  virtual void read(std::string &buf,
                    unsigned long long sz, unsigned long long off);
  virtual void get_attr(extent_protocol::attr &a);
  virtual void set_attr(extent_protocol::attr &a, unsigned char mask,
                        extent_protocol::attr &attr);

  std::string buf;
  extent_protocol::attr attr;
};

class extent_server
{
  std::map<extent_protocol::extentid_t, extent *> *extents;
  pthread_mutex_t mutex;

public:
  extent_server();
  virtual ~extent_server();

  virtual int put(extent_protocol::extentid_t id,
                  std::string, unsigned long long off,
                  extent_protocol::attr &);
  virtual int get(extent_protocol::extentid_t id,
                  unsigned long long sz, unsigned long long off,
                  std::string &);
  virtual int getattr(extent_protocol::extentid_t id, extent_protocol::attr &a);
  virtual int setattr(extent_protocol::extentid_t id, extent_protocol::attr a,
                      unsigned char mask, extent_protocol::attr &attr);
  virtual int remove(extent_protocol::extentid_t id, int &);
};

#endif







