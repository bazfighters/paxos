#ifndef yfs_client_h
#define yfs_client_h

#include <string>
//#include "yfs_protocol.h"
#include "extent_client.h"
#include "lock_client.h"
#include <vector>
#include <assert.h>

#define DIR_CONT_DEL "\n"

#define YFS_SIZE_MASK  EXTENT_SIZE_MASK
#define YFS_ATIME_MASK EXTENT_ATIME_MASK
#define YFS_MTIME_MASK EXTENT_MTIME_MASK

#define ROOT_INUM 0x00000001

#define inum_random() ((unsigned long long) random())

class yfs_client
{
  extent_client *ec;
  lock_client *lc;
public:
  typedef unsigned long long inum;
  enum xxstatus { OK, RPCERR, NOENT, IOERR, FBIG };
  typedef int status;

  struct fileinfo {
    unsigned long long size;
    unsigned long atime;
    unsigned long mtime;
    unsigned long ctime;
  };
  struct dirinfo {
    unsigned long atime;
    unsigned long mtime;
    unsigned long ctime;
  };
  struct dirent {
    std::string name;
    inum in;
  };

private:
  static std::string filename(inum);
  static inum n2i(std::string);
  static std::string i2n(inum);
  static inum todir(inum);
  static inum tofile(inum);
  static void dir_content_parser(std::string, std::list<dirent> &);
  static std::string dir_content_encoder(std::list<dirent> &);

public:
  yfs_client(std::string, std::string);

  bool isfile(inum);
  bool isdir(inum);

  inum ilookup(inum di, std::string name);

  status create(inum, std::string, inum &, fileinfo &);
  status remove(inum, const char *);
  status createdir(inum, const char *, inum &, dirinfo &);

  status read(inum, char *&, size_t, off_t);
  status write(inum, const char *, size_t, off_t);

  status getfile(inum, fileinfo &);
  status setfile(inum, fileinfo &, unsigned char);

  status getdir(inum, dirinfo &);
  status readdir(inum, std::list<dirent> &);

  status lock(inum);
  status unlock(inum);
};

//#define ASSERT_LOCK

class ScopedFileLock
{

private:
  lock_client *lc;
  lock_protocol::lockid_t lid;
public:
  ScopedFileLock(lock_client *lc, lock_protocol::lockid_t lid):
    lc(lc), lid(lid) {
#ifdef ASSERT_LOCK
    assert(lc->acquire(lid) == lock_protocol::OK);
#else
    lc->acquire(lid);
#endif
  }

  ~ScopedFileLock() {
#ifdef ASSERT_LOCK
    assert(lc->release(lid) == lock_protocol::OK);
#else
    lc->release(lid);
#endif
  }
};

#endif
