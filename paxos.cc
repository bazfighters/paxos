#include "paxos.h"
#include "handle.h"
// #include <signal.h>
#include <stdio.h>

// This module implements the proposer and acceptor of the Paxos
// distributed algorithm as described by Lamport's "Paxos Made
// Simple".  To kick off an instance of Paxos, the caller supplies a
// list of nodes, a proposed value, and invokes the proposer.  If the
// majority of the nodes agree on the proposed value after running
// this instance of Paxos, the acceptor invokes the upcall
// paxos_commit to inform higher layers of the agreed value for this
// instance.


bool
operator> (const prop_t &a, const prop_t &b)
{
  return (a.n > b.n || (a.n == b.n && a.m > b.m));
}

bool
operator>= (const prop_t &a, const prop_t &b)
{
  return (a.n > b.n || (a.n == b.n && a.m >= b.m));
}

std::string
print_members(const std::vector<std::string> &nodes)
{
  std::string s;
  s.clear();
  for (unsigned i = 0; i < nodes.size(); i++) {
    s += nodes[i];
    if (i < (nodes.size()-1)) {
      s += ",";
    }
  }
  return s;
}

bool isamember(std::string m, const std::vector<std::string> &nodes)
{
  for (unsigned i = 0; i < nodes.size(); i++) {
    if (nodes[i] == m) {
      return 1;
    }
  }
  return 0;
}

bool
proposer::isrunning()
{
  bool r;
  assert(pthread_mutex_lock(&pxs_mutex)==0);
  r = !stable;
  assert(pthread_mutex_unlock(&pxs_mutex)==0);
  return r;
}

// check if the servers in l2 contains a majority of servers in l1
bool
proposer::majority(const std::vector<std::string> &l1,
                   const std::vector<std::string> &l2)
{
  unsigned n = 0;

  for (unsigned i = 0; i < l1.size(); i++) {
    if (isamember(l1[i], l2)) {
      n++;
    }
  }
  return n >= (l1.size() >> 1) + 1;
}

proposer::proposer(class paxos_change *_cfg, class acceptor *_acceptor,
                   std::string _me)
  : cfg(_cfg), acc (_acceptor), me (_me), break1 (false), break2 (false),
    stable (true)
{
  assert (pthread_mutex_init(&pxs_mutex, NULL) == 0);
  // sets up the last proposal used, which is none.
  my_n.n = 0;
  my_n.m = _me;
}

// updates my_n in order to be higher than the highest ever seen.
void
proposer::setn()
{
  my_n.n = acc->get_n_h().n + 1 > my_n.n + 1 ? acc->get_n_h().n + 1 : my_n.n + 1;
}

bool
proposer::run(int instance, std::vector<std::string> newnodes, std::string newv)
{
  //printf("proposer::run: called\n");
  std::vector<std::string> accepts;
  std::vector<std::string> nodes;
  std::vector<std::string> nodes1;
  std::string v;
  bool r = false;

  // needs to set the value we would like to propose to the given value.
  c_v = newv;

  // sets the nodes in the instance to the received nodes.
  c_nodes = newnodes;
  //printf("proposer::run: number of nodes in the election: %lu\n", newnodes.size());

  pthread_mutex_lock(&pxs_mutex);
  printf("proposer::run: initiate paxos for %s w. i=%d v=%s stable=%d\n",
         print_members(newnodes).c_str(), instance, newv.c_str(), stable);
  if (!stable) {  // already running proposer?
    //printf("proposer::run: already running\n");
    pthread_mutex_unlock(&pxs_mutex);
    return false;
  }

  // it is now running paxos
  stable = false;
  
  setn();
  accepts.clear();
  nodes.clear();
  v.clear();
  nodes = c_nodes;
  if (prepare(instance, accepts, nodes, v)) {
    if (majority(c_nodes, accepts)) {
      //printf("proposer::run: received a majority of prepare responses\n");

      if (v.size() == 0) {
        v = c_v;
      }

      breakpoint1();

      nodes1 = accepts;
      accepts.clear();
      accept(instance, accepts, nodes1, v);

      if (majority(c_nodes, accepts)) {
        //printf("proposer::run: received a majority of accept responses\n");
        breakpoint2();
        decide(instance, accepts, v);
        r = true;
      } else {
        //printf("proposer::run: no majority of accept responses\n");
      }
    } else {
      //printf("proposer::run: no majority of prepare responses\n");
    }
  } else {
    //printf("proposer::run: prepare is rejected %d\n", stable);
  }
  stable = true;
  pthread_mutex_unlock(&pxs_mutex);
  return r;
}

bool
proposer::prepare(unsigned instance, std::vector<std::string> &accepts,
                  std::vector<std::string> nodes,
                  std::string &v)
{
  //printf("proposer::prepare: called\n");
  bool prepared = false;

  // sets up the arguments to be sent
  paxos_protocol::preparearg arg;
  arg.instance = instance;
  arg.n = my_n;
  arg.v = v;

  // how should this be initialized?
  prop_t max_na;
  max_na.n = 0;
  max_na.m = "";

  // the response to be received
  paxos_protocol::prepareres res;

  int num_nodes = nodes.size();
  res.accept = 0;
  res.oldinstance = 0;

  // send an rpc to everyone to prepare the request
  for(int cur_node = 0; cur_node < num_nodes; cur_node++) {
    prepared = true;
    handle h(nodes[cur_node]);
    //printf("proposer::prepare: getting handle for node: %s\n", nodes[cur_node].c_str());
    rpcc *cl = h.get_rpcc();
    if(cl == NULL) {
      //printf("proposer::prepare: no handler to call %s\n", nodes[cur_node].c_str());
      continue;
    }

    //printf("proposer::prepare: calling preparereq\n");
    int ret = cl->call(paxos_protocol::preparereq, nodes[cur_node], arg, res, rpcc::to(RPC_TIMEOUT));

    if (ret != paxos_protocol::OK) {
      //printf("proposer::prepare: problem with %s returned error %d\n",
      //       nodes[cur_node].c_str(), ret);
      if(ret == rpc_const::atmostonce_failure || ret == rpc_const::oldsrv_failure)
        mgr.delete_handle(nodes[cur_node]);
      continue;
    }

    if(res.oldinstance) {
      //printf("proposer::prepare: got an old instance\n");
      // saves the old instance
      acc->commit(instance, res.v_a);
      // stops running paxos
      return false;
    }

    if(res.accept) {
      if(res.n_a > max_na) {
        max_na = res.n_a;
        v = res.v_a;
      }
      //printf("proposer::prepare: prepare accepted\n");
      accepts.push_back(nodes[cur_node]);
    } else {
      //printf("proposer::prepare: prepare not accepted\n");
    }
  }

  return prepared;
}


void
proposer::accept(unsigned instance, std::vector<std::string> &accepts,
                 std::vector<std::string> nodes, std::string v)
{
  //printf("proposer::accept: called\n");
  // sets up the arguments to be sent
  paxos_protocol::acceptarg arg;
  arg.instance = instance;
  arg.n = my_n;
  arg.v = v;

  int res;
  int num_nodes = nodes.size();

  //send to all an rpc to accept the request
  for(int cur_node = 0; cur_node < num_nodes; cur_node++) {
    handle h(nodes[cur_node]);

    rpcc *cl = h.get_rpcc();
    if(cl == NULL) {
      continue;
    }

    int ret = cl->call(paxos_protocol::acceptreq, nodes[cur_node], arg, res, rpcc::to(RPC_TIMEOUT));
    if (ret != paxos_protocol::OK) {
      //printf("proposer::accept: problem with %s returned error %d\n",
      //       nodes[cur_node].c_str(), ret);
      if(ret == rpc_const::atmostonce_failure || ret == rpc_const::oldsrv_failure)
        mgr.delete_handle(nodes[cur_node]);
      continue;
    }

    accepts.push_back(nodes[cur_node]);
  }

}

void
proposer::decide(unsigned instance, std::vector<std::string> accepts,
                 std::string v)
{
  //printf("proposer::decide: called\n");
  // sets up the arguments to be sent
  paxos_protocol::decidearg arg;
  arg.instance = instance;
  arg.v = v;

  int res;
  int num_nodes = accepts.size();

  //send to all an rpc to decide the request
  for(int cur_node = 0; cur_node < num_nodes; cur_node++) {
    handle h(accepts[cur_node]);

    rpcc *cl = h.get_rpcc();
    if(cl == NULL) {
      continue;
    }

    int ret = cl->call(paxos_protocol::decidereq, accepts[cur_node], arg, res, rpcc::to(RPC_TIMEOUT));
    if (ret != paxos_protocol::OK) {
      //printf("proposer::decide: problem with %s returned error %d\n",
      //       accepts[cur_node].c_str(), ret);
      if(ret == rpc_const::atmostonce_failure || ret == rpc_const::oldsrv_failure)
        mgr.delete_handle(accepts[cur_node]);
      continue;
    }
  }
}

acceptor::acceptor(class paxos_change *_cfg, bool _first, std::string _me,
                   std::string _value)
  : cfg(_cfg), me (_me), instance_h(0)
{
  assert (pthread_mutex_init(&pxs_mutex, NULL) == 0);

  n_h.n = 0;
  n_h.m = me;
  n_a.n = 0;
  n_a.m = me;
  v_a.clear();

  l = new log (this, me);

  if (instance_h == 0 && _first) {
    values[1] = _value;
    l->loginstance(1, _value);
    instance_h = 1;
  }

  pxs = new rpcs(atoi(_me.c_str()));
  pxs->reg(paxos_protocol::preparereq, this, &acceptor::preparereq);
  pxs->reg(paxos_protocol::acceptreq, this, &acceptor::acceptreq);
  pxs->reg(paxos_protocol::decidereq, this, &acceptor::decidereq);
}

paxos_protocol::status
acceptor::preparereq(std::string src, paxos_protocol::preparearg a,
                     paxos_protocol::prepareres &r)
{
  // acceptor prepare(instance, n) handler:
  //  if instance <= instance_h
  //    reply oldinstance(instance, instance_value)
  //  else if n > n_h
  //    n_h = n
  //    reply prepare_ok(n_a, v_a)

  //printf("acceptor::preparereq: called\n");
  assert(pthread_mutex_lock(&pxs_mutex) == 0);

  r.accept = 0;
  r.oldinstance = 0;

  if(a.instance <= instance_h) {
    r.oldinstance = 1;
    r.v_a = values[a.instance];
    //printf("acceptor::preparereq: old instance\n");
  } else if(a.n > n_h) {
    //printf("acceptor::preparereq: accepting the request\n");
    l->loghigh(a.n);
    r.accept = 1;
    n_h = a.n;
    r.n_a = n_a;
    r.v_a = v_a;
  } else {
    //printf("acceptor::preparereq: not accepting the request\n");
  }

  assert(pthread_mutex_unlock(&pxs_mutex) == 0);
  return paxos_protocol::OK;

}

paxos_protocol::status
acceptor::acceptreq(std::string src, paxos_protocol::acceptarg a, int &r)
{
  // acceptor accept(instance, n, v) handler:
  // if n >= n_h
  //  n_a = n
  //  v_a = v
  //  reply accept_ok(n)

  //printf("acceptor::acceptreq: called\n");
  assert(pthread_mutex_lock(&pxs_mutex) == 0);
  r = 0;

  if(a.instance < instance_h) {
    //printf("acceptor::acceptreq: old instance\n");
  } else if(a.n >= n_h) {
    //printf("acceptor::acceptreq: accepting the request\n");
    l->logprop(a.n, a.v);
    r = 1;
    n_a = a.n;
    v_a = a.v;
  } else {
    //printf("acceptor::acceptreq: not accepting the request\n");
  }
  
  assert(pthread_mutex_unlock(&pxs_mutex) == 0);
  return paxos_protocol::OK;
}

paxos_protocol::status
acceptor::decidereq(std::string src, paxos_protocol::decidearg a, int &r)
{
  // acceptor decide(instance, v) handler:
  //  paxos_commit(instance, v)
  paxos_protocol::status ret = paxos_protocol::OK;
  //printf("acceptor::decidereq: called\n");
  assert(pthread_mutex_lock(&pxs_mutex) == 0);
  if(a.instance > instance_h) {
    r = paxos_protocol::OK;
    commit_wo(a.instance, a.v);
  }
  assert(pthread_mutex_unlock(&pxs_mutex) == 0);
  return ret;
}

void
acceptor::commit_wo(unsigned instance, std::string value)
{
  //printf("acceptor::commit_wo: called\n");
  //assume pxs_mutex is held
  printf("acceptor::commit_wo: instance=%d has v=%s\n", instance, value.c_str());
  if (instance > instance_h) {
    //printf("acceptor::commit_wo: highest accept=%d\n", instance);
    values[instance] = value;
    l->loginstance(instance, value);
    instance_h = instance;
    n_h.n = 0;
    n_h.m = me;
    n_a.n = 0;
    n_a.m = me;
    v_a.clear();
    if (cfg) {
      pthread_mutex_unlock(&pxs_mutex);
      cfg->paxos_commit(instance, value);
      pthread_mutex_lock(&pxs_mutex);
    }
  }
}

void
acceptor::commit(unsigned instance, std::string value)
{
  pthread_mutex_lock(&pxs_mutex);
  commit_wo(instance, value);
  pthread_mutex_unlock(&pxs_mutex);
}

std::string
acceptor::dump()
{
  //printf("acceptor::dump: called\n");
  return l->dump();
}

void
acceptor::restore(std::string s)
{
  //printf("acceptor::restore: called\n");
  l->restore(s);
  l->logread();
}



// For testing purposes

// Call this from your code between phases prepare and accept of proposer
void
proposer::breakpoint1()
{
  if (break1) {
    printf("Dying at breakpoint 1!\n");
    exit(1);
  }
}

// Call this from your code between phases accept and decide of proposer
void
proposer::breakpoint2()
{
  if (break2) {
    printf("Dying at breakpoint 2!\n");
    exit(1);
  }
}

void
proposer::breakpoint(int b)
{
  if (b == 3) {
    printf("Proposer: breakpoint 1\n");
    break1 = true;
  } else if (b == 4) {
    printf("Proposer: breakpoint 2\n");
    break2 = true;
  }
}
