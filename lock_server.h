// Lock server interface

#ifndef lock_server_h
#define lock_server_h

#include <string>
#include <map>

#include "lock_protocol.h"
#include "lock_client.h"
#include "rpc.h"
#include "rsm.h"

#define LOCK_MAP_SZ 100

struct lock_server_request_id
{
  int clt;
  int seqno;
  lock_server_request_id(): clt(0), seqno(0) {}
  lock_server_request_id(int clt, int seqno):
  clt(clt), seqno(seqno) {}
};

struct lock_state
{
  int clt;
  int n_acquires;
  int state;
  pthread_mutex_t mutex;
  pthread_mutex_t marshal_mutex;
public:
  enum xxstate { LOCKED, FREE };
  typedef int l_state;
  lock_state();
  ~lock_state();
  int get_acquires();
  // returns the previous state of the lock
  l_state acquire(int clt);
  l_state release(int clt);
};

class lock_server : public rsm_state_transfer
{
private:
  class rsm *rsm;
  std::map<lock_protocol::lockid_t, lock_state *> locks;
  std::map<lock_protocol::lockid_t,lock_server_request_id *> request_history;
  pthread_mutex_t mutex;

public:
  lock_server(class rsm *rsm=0);
  virtual ~lock_server();

  virtual lock_protocol::status stat(int clt, lock_protocol::lockid_t lid, int &r);
  virtual lock_protocol::status acquire(int clt, lock_protocol::lockid_t lid, int seqno, int &r);
  virtual lock_protocol::status release(int clt, lock_protocol::lockid_t lid, int &r);
  virtual std::string marshal_state();
  virtual void unmarshal_state(std::string state);
};

inline marshall &operator<<(marshall &rep, lock_server_request_id id) {
  // lock any needed mutexes
  rep << id.clt;
  rep << id.seqno;
  // unlock any mutexes
  return rep;
}

inline unmarshall &operator>>(unmarshall &rep, lock_server_request_id &id) {

  // lock any needed mutexes
  rep >> id.clt;
  rep >> id.seqno;
  // unlock any mutexes
  return rep;
}

inline bool operator>(const lock_server_request_id o1, const lock_server_request_id o2) {
    return o1.clt > o2.clt? true : o1.seqno > o2.seqno;
};

inline bool operator<(const lock_server_request_id o1, const lock_server_request_id o2) {
    return o1.clt < o2.clt? true : o1.seqno < o2.seqno;
};

inline bool operator>=(const lock_server_request_id o1, const lock_server_request_id o2) {
    return o1.clt >= o2.clt? true : o1.seqno >= o2.seqno;
};

inline bool operator<=(const lock_server_request_id o1, const lock_server_request_id o2) {
    return o1.clt <= o2.clt? true : o1.seqno <= o2.seqno;
};

inline bool operator==(const lock_server_request_id o1, const lock_server_request_id o2) {
    return o1.clt == o2.clt && o1.seqno == o2.seqno;
};

inline bool operator!=(const lock_server_request_id o1, const lock_server_request_id o2) {
    return o1.clt != o2.clt? true : o1.seqno != o2.seqno;
};

inline marshall &operator<<(marshall &rep, lock_state state) {
  // lock any needed mutexes
  assert(pthread_mutex_lock(&state.marshal_mutex) == 0);
  rep << state.clt;
  rep << state.n_acquires;
  rep << state.state;
  assert(pthread_mutex_unlock(&state.marshal_mutex) == 0);
  // unlock any mutexes
  return rep;
}

inline unmarshall &operator>>(unmarshall &rep, lock_state &state) {

  // lock any needed mutexes
  assert(pthread_mutex_lock(&state.marshal_mutex) == 0);
  rep >> state.clt;
  rep >> state.n_acquires;
  rep >> state.state;
  assert(pthread_mutex_unlock(&state.marshal_mutex) == 0);
  // unlock any mutexes
  return rep;
}

#endif
