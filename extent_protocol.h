// extent wire protocol

#ifndef extent_protocol_h
#define extent_protocol_h

#include "rpc.h"

#define READ_ALL (~(0L))

#define EXTENT_SIZE_MASK  (1 << 0)
#define EXTENT_ATIME_MASK (1 << 1)
#define EXTENT_MTIME_MASK (1 << 2)


class extent_protocol
{
public:
  typedef int status;
  typedef unsigned long long extentid_t;
  enum xxstatus { OK, RPCERR, NOENT, IOERR, FBIG};
  enum rpc_numbers {
    put = 0x6001,
    get,
    getattr,
    setattr,
    remove
  };
  static const unsigned int maxextent = (1 << 23);

  struct attr {
    unsigned int atime;
    unsigned int mtime;
    unsigned int ctime;
    unsigned int size;
  };
};

inline unmarshall &
operator>>(unmarshall &u, extent_protocol::attr &a)
{
  u >> a.atime;
  u >> a.mtime;
  u >> a.ctime;
  u >> a.size;
  return u;
}

inline marshall &
operator<<(marshall &m, extent_protocol::attr a)
{
  m << a.atime;
  m << a.mtime;
  m << a.ctime;
  m << a.size;
  return m;
}

#endif
