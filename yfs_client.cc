// yfs client.  implements FS operations using extent and lock server
#include "yfs_client.h"
#include "extent_client.h"
#include <sstream>
#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

yfs_client::yfs_client(std::string extent_dst, std::string lock_dst)
{
  ec = new extent_client(extent_dst);
  lc = new lock_client(lock_dst);
}

yfs_client::inum
yfs_client::n2i(std::string n)
{
  std::istringstream ist(n);
  unsigned long long finum;
  ist >> std::hex >> finum;
  return finum;
}

std::string
yfs_client::i2n(inum in)
{
  char ins[sizeof(inum) << 1];

  sprintf(ins, "%016llx", in);
  return std::string(ins);
}

std::string
yfs_client::filename(inum inum)
{
  std::ostringstream ost;
  ost << inum;
  return ost.str();
}

bool
yfs_client::isfile(inum inum)
{
  return (inum & 0x80000000);
}

yfs_client::inum
yfs_client::todir(inum inum)
{
  return (inum & ~0x80000000);
}

yfs_client::inum
yfs_client::tofile(inum inum)
{
  return (inum | 0x80000000);
}

bool
yfs_client::isdir(inum inum)
{
  return !isfile(inum);
}

void
yfs_client::dir_content_parser(std::string data, std::list<dirent> &dir)
{
  size_t start = 0, pos = 0;
  std::string delimiter = DIR_CONT_DEL, in, name;

  if(!(data.compare(std::string("")))) {
    return;
  }

  while(true) {
    pos = data.find(delimiter, start);
    in = data.substr(start, sizeof(inum) << 1);
    start += sizeof(inum) << 1;

    name = data.substr(start, pos - start);
    start = pos + delimiter.length();

    dirent d;
    d.name = name;
    d.in = n2i(in);
    dir.push_back(d);

    if (pos == std::string::npos) {
      break;
    }
  }
}

std::string
yfs_client::dir_content_encoder(std::list<dirent> &dir)
{
  if(dir.empty()) {
    return "";
  }

  std::ostringstream buf;
  std::list<dirent>::iterator it, end = --dir.end();
  for(it = dir.begin(); it != end; it++) {
    buf << i2n(it->in) << it->name << DIR_CONT_DEL;
  }
  buf << i2n(it->in) << it->name;

  return buf.str();
}

yfs_client::status
yfs_client::create(inum parent, std::string name, inum &in, fileinfo &fin)
{
  printf("create file on dir %016llx\n", parent);
  // Check if name already exists
  std::list<dirent> dir;
  readdir(parent, dir);

  std::list<dirent>::iterator it;
  for(it = dir.begin(); it != dir.end(); it++) {
    if(!(it->name.compare(name))) {
      // File already exists
      in = it->in;
      extent_protocol::attr a;
      a.size = 0;
      if(ec->setattr(in, a, EXTENT_SIZE_MASK, a) != extent_protocol::OK) {
        return IOERR;
      }
      fin.atime = a.atime;
      fin.mtime = a.mtime;
      fin.ctime = a.ctime;
      fin.size = a.size;
      return OK;
    }
  }

  // Create new file
  in = tofile(inum_random()); // Random inum
  extent_protocol::attr a;
  if(ec->put(in, std::string(""), 0, a) != extent_protocol::OK) {
    return IOERR;
  }
  fin.atime = a.atime;
  fin.mtime = a.mtime;
  fin.ctime = a.ctime;
  fin.size = a.size;

  // Update directory
  dirent d;
  d.name = name;
  d.in = in;
  dir.push_back(d);
  if(ec->put(parent, dir_content_encoder(dir), 0, a) != extent_protocol::OK) {
    // Maybe try to delete file
    return IOERR;
  }

  return OK;
}

yfs_client::status
yfs_client::remove(inum parent, const char *name)
{
  printf("remove file on dir %016llx\n", parent);
  std::string fname(name);

  std::list<dirent> dir;
  inum in;
  status ret = readdir(parent, dir);
  if(ret != OK) {
    return ret;
  }

  // Deletes file from parent
  std::list<dirent>::iterator it;

  bool has_file = false;
  for(it = dir.begin(); it != dir.end(); it++) {
    if(!(it->name.compare(fname))) {
      if(isdir(it->in)) {
        return NOENT;
      }
      in = it->in;
      has_file = true;
      break;
    }
  }

  // Returns error if there was no such file
  if(!has_file) {
    return NOENT;
  }

  dir.erase(it);

  extent_protocol::attr a;
  a.size = 0;
  if(ec->setattr(parent, a, EXTENT_SIZE_MASK, a) != extent_protocol::OK) {
    return IOERR;
  }
  if(ec->put(parent, dir_content_encoder(dir), 0, a) != extent_protocol::OK) {
    return IOERR;
  }
  if(ec->remove(in) != extent_protocol::OK) {
    return IOERR;
  }

  return OK;
}

yfs_client::status
yfs_client::createdir(inum parent, const char *name, inum &inum, dirinfo &din)
{
  printf("create dir on dir %016llx\n", parent);
  std::string dir_name(name);

  // Check if name already exists
  std::list<dirent> dir;
  readdir(parent, dir);

  std::list<dirent>::iterator it;
  // Check for duplicate filenames
  for(it = dir.begin(); it != dir.end(); it++) {
    if(!(it->name.compare(dir_name))) {
      // Maybe should overwrite
      return IOERR;
    }
  }

  // Create new dir
  inum = todir(inum_random()); // Random inum
  while(inum == ROOT_INUM) {
    inum = todir(inum_random());
  }

  extent_protocol::attr a;
  if(ec->put(inum, std::string(""), 0, a) != extent_protocol::OK) {
    return IOERR;
  }

  din.atime = a.atime;
  din.mtime = a.mtime;
  din.ctime = a.ctime;

  // Update directory
  dirent d;
  d.name = dir_name;
  d.in = inum;
  dir.push_back(d);
  if(ec->put(parent, dir_content_encoder(dir), 0, a) != extent_protocol::OK) {
    return IOERR;
  }

  return OK;
}

yfs_client::status
yfs_client::getfile(inum inum, fileinfo &fin)
{
  printf("getfile %016llx\n", inum);
  extent_protocol::attr a;
  if(ec->getattr(inum, a) != extent_protocol::OK) {
    return IOERR;
  }

  fin.atime = a.atime;
  fin.mtime = a.mtime;
  fin.ctime = a.ctime;
  fin.size = a.size;
  printf("getfile %016llx -> sz %llu\n", inum, fin.size);

  return OK;
}

yfs_client::status
yfs_client::setfile(inum inum, fileinfo &fin, unsigned char mask)
{
  printf("setattr: %016llx\n", inum);
  extent_protocol::attr a;
  a.atime = fin.atime;
  a.mtime = fin.mtime;
  a.ctime = fin.ctime;
  a.size = fin.size;
  if(ec->setattr(inum, a, mask, a) != extent_protocol::OK) {
    return IOERR;
  }
  return OK;
}

yfs_client::status
yfs_client::getdir(inum inum, dirinfo &din)
{
  printf("getdir %016llx\n", inum);
  extent_protocol::attr a;
  if(ec->getattr(inum, a) != extent_protocol::OK) {
    return IOERR;
  }
  din.atime = a.atime;
  din.mtime = a.mtime;
  din.ctime = a.ctime;

  return OK;
}

yfs_client::status
yfs_client::readdir(inum inum, std::list<dirent> &dir)
{
  printf("readdir %016llx\n", inum);
  std::string data;
  if(ec->get(inum, READ_ALL, 0, data) != extent_protocol::OK) {
    return IOERR;
  }
  dir_content_parser(data, dir);
  return OK;
}

yfs_client::status
yfs_client::read(inum inum, char *&buf, size_t size, off_t off)
{
  printf("read: %016llx\n", inum);
  std::string file;
  if(size > 0) {
    if(ec->get(inum, size, off, file) != extent_protocol::OK) {
      return IOERR;
    }
  }
  // changes the size to be equal to the read size
  size = file.size();
  buf = (char *) calloc(size, sizeof(char));
  strcpy(buf, file.c_str());
  return OK;
}

yfs_client::status
yfs_client::write(inum inum, const char *data, size_t size, off_t off)
{
  printf("write: %016llx\n", inum);
  std::string buf(data, size);

  extent_protocol::attr attr;
  if(ec->put(inum, buf, off, attr) != extent_protocol::OK) {
    return IOERR;
  }
  return OK;
}

yfs_client::status
yfs_client::lock(inum inum)
{
  assert(lc->acquire(inum) == lock_protocol::OK);
  return OK;
}

yfs_client::status
yfs_client::unlock(inum inum)
{
  assert(lc->release(inum) == lock_protocol::OK);
  return OK;
}
